-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2013 年 09 月 03 日 07:03
-- 服务器版本: 5.5.32
-- PHP 版本: 5.3.10-1ubuntu3.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `score`
--

-- --------------------------------------------------------

--
-- 表的结构 `action`
--

CREATE TABLE IF NOT EXISTS `action` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `tid` varchar(4) COLLATE utf8_general_mysql500_ci NOT NULL,
  `description` text COLLATE utf8_general_mysql500_ci,
  `title` text COLLATE utf8_general_mysql500_ci NOT NULL,
  `startDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `checkDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `endDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `score` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `action`
--

INSERT INTO `action` (`id`, `tid`, `description`, `title`, `startDate`, `checkDate`, `endDate`, `score`) VALUES
(1, '0000', '<table border="1" cellpadding="1" cellspacing="1" style="width:500px">\n	<tbody>\n		<tr>\n			<td>', 'adfa', '2013-09-02 15:04:49', '2012-12-31 16:00:00', '2017-12-30 16:00:00', 10);

-- --------------------------------------------------------

--
-- 表的结构 `daily`
--

CREATE TABLE IF NOT EXISTS `daily` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tid` varchar(4) COLLATE utf8_general_mysql500_ci NOT NULL,
  `sid` varchar(15) COLLATE utf8_general_mysql500_ci NOT NULL,
  `actionId` int(15) NOT NULL,
  `reason` text COLLATE utf8_general_mysql500_ci,
  `flag` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `DB`
--

CREATE TABLE IF NOT EXISTS `DB` (
  `id` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `userName` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_mysql500_ci NOT NULL,
  `userPwd` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_mysql500_ci NOT NULL,
  `driverClass` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_mysql500_ci NOT NULL,
  `url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_mysql500_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `flag` bit(1) NOT NULL DEFAULT b'1',
  `current` int(4) NOT NULL DEFAULT '0',
  `userId` varchar(255) NOT NULL,
  `serverId` varchar(255) NOT NULL,
  `accessable` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `userName` (`userName`,`userPwd`,`url`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `DB`
--

INSERT INTO `DB` (`id`, `userName`, `userPwd`, `driverClass`, `url`, `date`, `flag`, `current`, `userId`, `serverId`, `accessable`) VALUES
(00001, 'root', 'ufowzh13524645', 'com.mysql.jdbc.Driver', 'jdbc:mysql://localhost/', '2013-05-21 14:33:22', b'1', 0, '', '', 0);

-- --------------------------------------------------------

--
-- 表的结构 `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` varchar(9) COLLATE utf8_general_mysql500_ci NOT NULL,
  `userName` varchar(15) COLLATE utf8_general_mysql500_ci NOT NULL,
  `userPwd` varchar(15) COLLATE utf8_general_mysql500_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `flag` bit(1) NOT NULL DEFAULT b'0',
  `DB` int(5) DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8_general_mysql500_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- 转存表中的数据 `user`
--

INSERT INTO `user` (`id`, `userName`, `userPwd`, `date`, `flag`, `DB`, `telephone`) VALUES
('0000', 'ly', 'ly', '2013-07-21 01:49:24', b'1', NULL, NULL),
('000000', 'mis', 'mis', '2013-07-21 01:48:01', b'0', NULL, NULL),
('000000000', 'zfl', 'zfl', '2013-07-21 01:48:01', b'0', NULL, NULL),
('000000001', 'zfm', '123456', '2013-08-18 07:07:32', b'0', NULL, NULL),
('000000002', 'gmq', '123456', '2013-08-18 07:07:32', b'0', NULL, NULL),
('000000003', 'wz', '123456', '2013-08-18 07:09:23', b'0', NULL, NULL),
('000000004', 'cy', '123456', '2013-08-18 07:09:23', b'0', NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
