package com.logistics.File;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.DAO.User;
import com.opensymphony.xwork2.ActionSupport;

public class Uploader extends ActionSupport implements ServletRequestAware{
	private static final long serialVersionUID = 1L;
	/**
	 * @title Uploader.java
	 * @package com.logistics.File
	 * @author ufo
	 */

	private HttpServletRequest request;
	private File file;
	private String savePath;
	private InputStream inputStream;
	private String basicPath;

	protected Configuration conf=new Configuration().configure();
	protected SessionFactory sf=conf.buildSessionFactory();
	protected Session hibernateSess=sf.openSession();
	protected User user;

	public String upload(){
		if(isOnline()){
			addSessionToUser();
			try {
				FileOutputStream fos=null;
				String name;
				boolean flag=true;
				do{
					Random target=new Random();
					char[] numbersAndLetters=("0123456789abcdefghijklmnopqrstuvwxyz" +
			                "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
					char [] randBuffer = new char[4];
					for(int i=0; i<randBuffer.length; i++)
						randBuffer[i] = numbersAndLetters[target.nextInt(71)];
					name=new String(randBuffer)+user.getId();
					if(!(new File(getSavePath()+name).exists())){
						fos=new FileOutputStream(getSavePath()+name);
						flag=false;
					}
				}while(flag);
				FileInputStream fis=new FileInputStream(getFile());
				byte[] buffer=new byte[1024];
				int len=0;
				while((len=fis.read(buffer))>0){
					fos.write(buffer,0,len);
				}
				String html="<html xmlns='http://www.w3.org/1999/xhtml'>"
						+"<meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><head><title></title></head>"
						+"<script type='text/javascript' src='"
						+getBasicPath()+
						"js/common.js'></script>"
						+"<script type='text/javascript' src='"
						+getBasicPath()+
						"js/prototype.js'></script><script type='text/javascript'>"
						+"function onloadTmp(){setCookie('load','"
						+name
						+"');}</script><body onload='onloadTmp();'><img src='"+getBasicPath()+"fileDownload?name="
						+name
						+"'/></body></html>";
				inputStream=new ByteArrayInputStream((html).getBytes());
				return SUCCESS;
			} catch (Exception e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
				return ERROR;
			}
		}
		return ERROR;
	}
	public boolean isOnline(){
		if(request.getSession().getAttribute("userId")!=null)
			return true;
		else 
			return false;
	}
	private void addSessionToUser(){
		user=(User) hibernateSess.load(User.class, (Serializable) request.getSession().getAttribute("userId"));
	}
	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO 自动生成的方法存根
		this.setRequest(request);
	}
	public HttpServletRequest getRequest() {
		return request;
	}
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}
	public String getSavePath() {
		return savePath;
	}
	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	public InputStream getResult() {
		return inputStream;
	}
	public String getBasicPath() {
		return basicPath;
	}
	public void setBasicPath(String basicPath) {
		this.basicPath = basicPath;
	}
}
