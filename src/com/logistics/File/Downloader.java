package com.logistics.File;

import java.io.FileInputStream;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.DAO.User;
import com.opensymphony.xwork2.ActionSupport;

public class Downloader extends ActionSupport implements ServletRequestAware{
	private static final long serialVersionUID = 1L;
	/**
	 * @title Downloader.java
	 * @package com.logistics.File
	 * @author ufo
	 */

	private HttpServletRequest request;
	private String savePath;
	private InputStream inputStream;

	protected Configuration conf=new Configuration().configure();
	protected SessionFactory sf=conf.buildSessionFactory();
	protected Session hibernateSess=sf.openSession();
	protected User user;

	public String download(){
		if(isOnline()){
			try {
				String name=request.getParameterValues("name")[0];
				if(!name.isEmpty()){
					FileInputStream fis=new FileInputStream(getSavePath()+name);
					inputStream=fis;
				}
				return SUCCESS;
			} catch (Exception e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
				return ERROR;
			}
		}
		return ERROR;
	}
	public boolean isOnline(){
		if(request.getSession().getAttribute("userId")!=null)
			return true;
		else 
			return false;
	}
	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO 自动生成的方法存根
		this.setRequest(request);
	}
	public HttpServletRequest getRequest() {
		return request;
	}
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	public String getSavePath() {
		return savePath;
	}
	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	public InputStream getResult() {
		return inputStream;
	}
}
