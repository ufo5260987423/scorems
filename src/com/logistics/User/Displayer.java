package com.logistics.User;

import java.util.Map;

import org.apache.struts2.json.annotations.JSON;

import com.logistic.Basic;

public class Displayer extends Basic{

	private static final long serialVersionUID = 1L;
	/**
	 * @title Displayer.java
	 * @package com.logistics.User
	 * @author ufo
	 */

	private String[] id;

	public String display(){
		if(isOnline()){
			addSessionToUser();
			result.put("id", user.getId());
			result.put("userName", user.getUserName());
			result.put("actionFlag", user.getFlag().toString());
			result.put("date", user.getDate());
			result.put("flag", "true");
			return SUCCESS;
		}
		result.put("flag", "false");
		return SUCCESS;
	}
@JSON(name="result")
	public Map<String,String> getResult() {
		return result;
	}
	public String[] getId() {
		return id;
	}
	public void setId(String[] id) {
		this.id = id;
	}

}
