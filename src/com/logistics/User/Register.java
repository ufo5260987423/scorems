package com.logistics.User;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.json.annotations.JSON;

import com.DAO.User;
import com.logistic.Basic;

public class Register extends Basic{
	private static final long serialVersionUID = 1L;
	/**
	 * @Title Register.java
	 * @Package com.logistics.User
	 * @author ufo
	 */
	
	private String[] id;
	private String[] userPwd;
	
	public String logout(){
		if(request.getSession().getAttribute("userId")!=null){
			request.getSession().removeAttribute("userId");
			result.put("flag", "true");
			return SUCCESS;
		}
		result.put("flag", "false");
		return SUCCESS;
	}
	public String changePwd(){
		if(isOnline()
				&&getUserPwd()[1].equals(getUserPwd()[2])
				&&request.getSession().getAttribute("userId").equals(getId()[0])){
			user=(User) hibernateSess.load(User.class, getId()[0]);
			if(!user.getUserPwd().equals(getUserPwd()[0])){
				result.put("flag", "false");
				return SUCCESS;
			}
			user.setUserPwd(getUserPwd()[1]);
			hibernateSess.save(user);
			tx.commit();
			result.put("flag","true");
			return SUCCESS;
		}else
			result.put("flag", "false");
			return SUCCESS;
	}
	public String login(){
		user=(User) hibernateSess.load(User.class, getId()[0]);
		if(user.getUserPwd().trim().equals(getUserPwd()[0])){
			addUserToSession();

			result.put("userId", user.getId());
			result.put("userName",user.getUserName());
			result.put("actionFlag", user.getFlag().toString());
			result.put("date", user.getDate());
			result.put("flag", "true");
			return SUCCESS;
		}
		result.put("flag", "false");
		return SUCCESS;
	}
	public boolean isOnline(){
		if(request.getSession().getAttribute("userId")!=null)
			return true;
		else 
			return false;
	}
	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO 自动生成的方法存根
		this.request=request;
	}
	public String[] getId() {
		return id;
	}
	public void setId(String[] id) {
		this.id = id;
	}
	public String[] getUserPwd() {
		return userPwd;
	}
	public void setUserPwd(String[] userPwd) {
		this.userPwd = userPwd;
	}
	private void addUserToSession(){
		request.getSession().setAttribute("userId", user.getId());
	}
	@JSON(name="result")
	public Map<String,String> getResult() {
		return result;
	}
}
