package com.logistics.Daily;

import java.util.Map;

import org.apache.struts2.json.annotations.JSON;

import com.DAO.Daily;
import com.logistic.Basic;

public class Deleter extends Basic{
	private static final long serialVersionUID = 1L;
	/**
	 * @title Deleter.java
	 * @package com.logistics.Daily
	 * @author ufo
	 */
	private String[] id;
	private String[] currentDate;//形式均为yyyy-mm-dd hh:mm:ss
	
	protected Daily daily;

	public String delete(){
		if(isOnline()&&getCurrentDate()[0]!=null)
		{
			addSessionToUser();
			if(user.getFlag()==0){
				try{
				daily=(Daily)hibernateSess.load(Daily.class, getId()[0]);
				daily.setActionId("0");
				}
				catch(Exception e){
					result.put("flag", "false");
					return SUCCESS;
				}
				hibernateSess.save(daily);
				tx.commit();
				result.put("flag", "true");
				return SUCCESS;
			}
		}
		result.put("flag", "false");
		return SUCCESS;
	}
	@JSON(name="result")
	public Map<String,String> getResult() {
		return result;
	}
	public String[] getId() {
		return id;
	}
	public void setId(String[] id) {
		this.id = id;
	}
	public String[] getCurrentDate() {
		return currentDate;
	}
	public void setStartDate(String[] currentDate) {
		this.currentDate = currentDate;
	}
}
