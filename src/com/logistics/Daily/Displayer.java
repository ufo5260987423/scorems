package com.logistics.Daily;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.json.annotations.JSON;

import com.DAO.Action;
import com.DAO.Daily;
import com.logistic.Basic;

public class Displayer extends Basic{

	private static final long serialVersionUID = 1L;
	/**
	 * @title Displayer.java
	 * @package com.logistics.Daily
	 * @author ufo
	 */

	private String[] startDate;
	private String[] endDate;
	private String[] actionId;
	private String[] flag;
	private String[] first;
	private String[] max;

	protected Action action;
	protected Daily daily;

	public String display(){
		if(isOnline()){
			addSessionToUser();
			StringBuffer quary=new StringBuffer().append("from daily where");
			if((!getStartDate()[0].isEmpty())&&(!getEndDate()[0].isEmpty())){
				if(!quary.equals("from daily where"))
					quary.append(" and");
				quary.append(" date>='"+getStartDate()[0]+"' and date<='"+getEndDate()[0]+"'");
			}
			if(!getActionId()[0].isEmpty()){
				if(!quary.equals("from daily where"))
					quary.append(" and");
				quary.append(" actionId="+getActionId()[0]);
			}
			if(!quary.equals("from daily where"))
				quary.append(" and");
			if(user.getFlag()==1)
				quary.append(" tid="+request.getSession().getAttribute("userId"));
			else
				quary.append(" sid="+request.getSession().getAttribute("userId"));
			if(!quary.equals("from daily where"))
				quary.append(" and");
			if(getFlag()[0].equals("1"))
				quary.append(" and flag=1");
			else
				quary.append(" and flag=0");

			List list=hibernateSess.createQuery(quary.toString())
					.setFirstResult(Integer.parseInt(getFirst()[0]))
					.setMaxResults(Integer.parseInt(getMax()[0]))
					.list();
			Iterator i=list.iterator();
			int a=0;
			for(;i.hasNext();a++){
				daily=(Daily) i.next();

				result.put("dailyId:"+a, daily.getId());
				result.put("dailyDate:"+a,daily.getDate());
				result.put("dailySid:"+a,daily.getSid());
				result.put("dailyActionId:"+a, daily.getActionId());
				result.put("dailyReason:"+a, daily.getReason());
				result.put("dailyFlag:"+a, daily.getFlag().toString());
			}
			if(a>0){
				result.put("flag","true");
				return SUCCESS;
			}
		}
		result.put("flag", "false");
		return SUCCESS;
	}
	@JSON(name="result")
	public Map<String,String> getResult() {
		return result;
	}
	public String[] getStartDate() {
		return startDate;
	}
	public void setStartDate(String[] startDate) {
		this.startDate = startDate;
	}
	public String[] getEndDate() {
		return endDate;
	}
	public void setEndDate(String[] endDate) {
		this.endDate = endDate;
	}
	public String[] getActionId() {
		return actionId;
	}
	public void setActionId(String[] actionId) {
		this.actionId = actionId;
	}
	public String[] getFlag() {
		return flag;
	}
	public void setFlag(String[] flag) {
		this.flag = flag;
	}
	public String[] getFirst() {
		return first;
	}
	public void setFirst(String[] first) {
		this.first = first;
	}
	public String[] getMax() {
		return max;
	}
	public void setMax(String[] max) {
		this.max = max;
	}
}