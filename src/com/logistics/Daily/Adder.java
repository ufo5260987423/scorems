package com.logistics.Daily;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.struts2.json.annotations.JSON;

import com.DAO.Action;
import com.DAO.Daily;
import com.logistic.Basic;

public class Adder extends Basic{

	private static final long serialVersionUID = 1L;
	/**
	 * @title Adder.java
	 * @package com.logistics.Daily
	 * @author ufo
	 */
	
	private String[] reason;
	private String[] actionId;
	private String[] dailyId;
	private String[] flag;

	protected Action action;
	protected Daily daily;

	public String checkAttendence(){
		if(isOnline()){
			List list=hibernateSess.createQuery("from Daily where reason= :reason and actionId= :actionId and flag=0")
					.setString("reason", getReason()[0])
					.setString("actionId", getActionId()[0])
					.list();
			Iterator i=list.iterator();
			addSessionToUser();
			if(user.getFlag()!=1){
				result.put("flag", "false");
				return SUCCESS;
			}
			if(i.hasNext()){
				daily=(Daily)i.next();
				daily.setSid(user.getId());
				daily.setFlag((byte) 1);

				hibernateSess.save(daily);
				result.put("flag", "true");
				return SUCCESS;
				}
			
		}
		result.put("flag", "false");
		return SUCCESS;
	}
	public String randomNumberGenerate(){
		if(isOnline()){
			addSessionToUser();
			if(user.getFlag()==1){
				Random target=new Random();
				char[] numbersAndLetters=("0123456789abcdefghijklmnopqrstuvwxyz" +
		                "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
				char [] randBuffer = new char[4];
				for(int i=0; i<randBuffer.length; i++)
					randBuffer[i] = numbersAndLetters[target.nextInt(71)];

				reason=new String[1];
				reason[0]=new String(randBuffer);
				if(add().equals(SUCCESS)){
					result.put("randomString", reason[0]);
					result.put("flag", "true");
					return SUCCESS;
				}
				
			}
		}
		result.put("flag", "false");
		return SUCCESS;
	}
	public String check(){
		if(isOnline()){
			addSessionToUser();
			if(user.getFlag()!=1||getFlag()[0].isEmpty()
					||!(getFlag()[0].equals("1")&&getFlag()[0].equals("0"))){
				result.put("flag", "false");
				return SUCCESS;
			}
			daily=(Daily) hibernateSess.load(Daily.class, getDailyId()[0]);
			daily.setFlag(Byte.parseByte(getFlag()[0]));

			hibernateSess.save(daily);
			tx.commit();
			result.put("flag", "true");
			return SUCCESS;
		}
		result.put("flag", "false");
		return SUCCESS;
	}
	public String alt(){
		if(isOnline()){
			addSessionToUser();
			daily=(Daily) hibernateSess.load(Daily.class, getDailyId()[0]);
			if(user.getId().equals(daily.getId())){
				daily.setActionId(getActionId()[0]);
				daily.setReason(getReason()[0]);

				hibernateSess.save(daily);
				tx.commit();
				result.put("flag", "true");
				return SUCCESS;
			}	
		}
		result.put("flag", "false");
		return SUCCESS;
	}
	public String add(){
		if(isOnline()){
			addSessionToUser();
			addAction();
			daily=new Daily();
			daily.setActionId(action.getId());
			if(user.getFlag()==0)
				daily.setSid(user.getId());

			daily.setReason(getReason()[0]);

			hibernateSess.save(daily);
			tx.commit();

			result.put("dailyId", daily.getId());
			result.put("flag", "true");
			return SUCCESS;
		}
		result.put("flag", "false");
		return SUCCESS;
	}
	@JSON(name="result")
	public Map<String,String> getResult() {
		return result;
	}
	public String[] getReason() {
		return reason;
	}
	public void setReason(String[] reason) {
		this.reason = reason;
	}
	private void addAction(){
//		if(!getActionId()[0].equals("0"))
			action=(Action) hibernateSess.load(Action.class,getActionId()[0]);
//		else{
//			com.logistics.Action.Adder adder=new com.logistics.Action.Adder();
//			action=adder.add((String)request.getSession().getAttribute("userId"));
//		}
	}
	public String[] getActionId() {
		return actionId;
	}
	public void setActionId(String[] actionId) {
		this.actionId = actionId;
	}
	public String[] getDailyId() {
		return dailyId;
	}
	public void setDailyId(String[] dailyId) {
		this.dailyId = dailyId;
	}
	public String[] getFlag() {
		return flag;
	}
	public void setFlag(String[] flag) {
		this.flag = flag;
	}
}
