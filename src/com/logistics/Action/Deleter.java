package com.logistics.Action;

import java.util.Map;
import org.apache.struts2.json.annotations.JSON;
import com.DAO.Action;
import com.logistic.Basic;

public class Deleter extends Basic{
	private static final long serialVersionUID = 1L;
	/**
	 * @title Deleter.java
	 * @package com.logistics.Action
	 * @author ufo
	 */

	private String[] id;
	private String[] currentDate;//形式均为yyyy-mm-dd hh:mm:ss
	
	protected Action action;

	public String delete(){
		if(isOnline()&&getCurrentDate()[0]!=null)
		{
			addSessionToUser();
			if(user.getFlag()==1){
				try{
				action=(Action)hibernateSess.load(Action.class, getId()[0]);
				}
				catch(Exception e){
					result.put("flag", "false");
					return SUCCESS;
				}
				action.setCheckDate(getCurrentDate()[0]);
				action.setEndDate(getCurrentDate()[0]);
				action.setStartDate(getCurrentDate()[0]);

				hibernateSess.save(action);
				tx.commit();
				result.put("flag", "true");
				return SUCCESS;
			}
		}
		result.put("flag", "false");
		return SUCCESS;
	}
	@JSON(name="result")
	public Map<String,String> getResult() {
		return result;
	}
	public String[] getId() {
		return id;
	}
	public void setId(String[] id) {
		this.id = id;
	}
	public String[] getCurrentDate() {
		return currentDate;
	}
	public void setStartDate(String[] currentDate) {
		this.currentDate = currentDate;
	}
}
