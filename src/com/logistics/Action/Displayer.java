package com.logistics.Action;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.struts2.json.annotations.JSON;
import org.hibernate.Query;

import com.DAO.Action;
import com.logistic.Basic;

public class Displayer extends Basic{

	private static final long serialVersionUID = 1L;
	/**
	 * @title Displayer.java
	 * @package com.logistics.Action
	 * @author ufo
	 */

	private String[] id;
	private String[] title;
	private String[] topStartDate;
	private String[] bottomStartDate;
	private String[] topCheckDate;
	private String[] bottomCheckDate;
	private String[] topEndDate;
	private String[] bottomEndDate;
	private String[] first;
	private String[] max;

	protected Action action;

	private List select(){
		StringBuffer query=new StringBuffer().append("from action where");
		if(!getId()[0].isEmpty())
			query.append(" id="+getId()[0]);
		
		if((!getTopStartDate()[0].isEmpty())&&(!getBottomStartDate()[0].isEmpty())){
			if(!query.toString().equals("from action where"))
				query.append(" and");
			query.append(" startDate<='"+getTopStartDate()[0]+"' and startDate>='"+getBottomStartDate()[0]+"'");
		}
		if((!getTopCheckDate()[0].isEmpty())&&(!getBottomCheckDate()[0].isEmpty())){
			if(!query.toString().equals("from action where"))
				query.append(" and");
			query.append(" checkDate<='"+getTopCheckDate()[0]+"' and checkDate>='"+getBottomCheckDate()[0]+"'");
		}
		if((!getTopEndDate()[0].isEmpty())&&(!getBottomEndDate()[0].isEmpty())){
			if(!query.toString().equals("from daily where"))
				query.append(" and");
			query.append(" endDate<='"+getTopEndDate()[0]+"' and endDate>='"+getBottomEndDate()[0]+"'");
		}
		if(!getTitle()[0].isEmpty()){
			if(!query.toString().equals("from daily where"))
				query.append(" and");
			query.append(" title="+getTitle()[0]);
		}
		if(user.getFlag()==1){
			if(!query.toString().equals("from daily where"))
				query.append(" and");
			query.append(" tid="+request.getSession().getAttribute("userId"));
		}
		/*return hibernateSess.createQuery(query.toString())
				.setFirstResult(Integer.parseInt(getFirst()[0]))
				.setMaxResults(Integer.parseInt(getMax()[0]))
				.list();
		*/System.out.println(0.1);
		System.out.println(query.toString());
		Query query1=hibernateSess.createQuery(query.toString());
		System.out.println(0.2);
		return query1
				.setFirstResult(Integer.parseInt(getFirst()[0]))
				.setMaxResults(Integer.parseInt(getMax()[0]))
				.list();
	}
	public String display(){
		if(isOnline()){
			addSessionToUser();
			Iterator i=select().iterator();
			int a=0;
			for(;i.hasNext();a++){
				action=(Action) i.next();

				result.put("actionId:"+a, action.getId());
				result.put("actionTid:"+a,action.getTid());
				result.put("actionDescription:"+a,action.getDescription());
				result.put("actionGroup:"+a, action.getTitle());
				result.put("actionStartDate:"+a, action.getStartDate());
				result.put("actionCheckDate:"+a, action.getCheckDate());
				result.put("actionEndDate:"+a, action.getEndDate());
			}
			if(a>0){
				result.put("flag", "true");
				return SUCCESS;
			}
		}
		result.put("flag", "false");
		return SUCCESS;
	}
	public String displayTitle(){
		if(isOnline()){
			addSessionToUser();
			System.out.println("1");
			Iterator i=select().iterator();System.out.println("2");
			int a=0;
			for(;i.hasNext();a++){
				action=(Action) i.next();

				result.put("actionId:"+a, action.getId());
				result.put("actionTid:"+a,action.getTid());
				result.put("actionGroup:"+a, action.getTitle());
				result.put("actionStartDate:"+a, action.getStartDate());
				result.put("actionCheckDate:"+a, action.getCheckDate());
				result.put("actionEndDate:"+a, action.getEndDate());
			}
			if(a>0)return SUCCESS;
		}
		result.put("flag", "false");
		return SUCCESS;
	}
	@JSON(name="result")
	public Map<String,String> getResult() {
		return result;
	}
	public String[] getTopStartDate() {
		return topStartDate;
	}
	public void setTopStartDate(String[] topStartDate) {
		this.topStartDate = topStartDate;
	}
	public String[] getBottomStartDate() {
		return bottomStartDate;
	}
	public void setBottomStartDate(String[] bottomStartDate) {
		this.bottomStartDate = bottomStartDate;
	}
	public String[] getTopCheckDate() {
		return topCheckDate;
	}
	public void setTopCheckDate(String[] topCheckDate) {
		this.topCheckDate = topCheckDate;
	}
	public String[] getBottomCheckDate() {
		return bottomCheckDate;
	}
	public void setBottomCheckDate(String[] bottomCheckDate) {
		this.bottomCheckDate = bottomCheckDate;
	}
	public String[] getTopEndDate() {
		return topEndDate;
	}
	public void setTopEndDate(String[] topEndDate) {
		this.topEndDate = topEndDate;
	}
	public String[] getBottomEndDate() {
		return bottomEndDate;
	}
	public void setBottomEndDate(String[] bottomEndDate) {
		this.bottomEndDate = bottomEndDate;
	}
	public String[] getId() {
		return id;
	}
	public void setId(String[] id) {
		this.id = id;
	}
	public String[] getFirst() {
		return first;
	}
	public void setFirst(String[] first) {
		this.first = first;
	}
	public String[] getMax() {
		return max;
	}
	public void setMax(String[] max) {
		this.max = max;
	}
	public String[] getTitle() {
		return title;
	}
	public void setTitle(String[] title) {
		this.title = title;
	}
}