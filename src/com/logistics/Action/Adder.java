package com.logistics.Action;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.struts2.json.annotations.JSON;

import com.DAO.Action;
import com.logistic.Basic;

public class Adder extends Basic{
	private static final long serialVersionUID = 1L;
	/**
	 * @title Adder.java
	 * @package com.logistics.Action
	 * @author ufo
	 */

	private String[] id;
	private String[] tid;
	private String[] description;
	private String[] title;
	private String[] startDate;//形式均为yyyy-mm-dd hh:mm:ss
	private String[] checkDate;
	private String[] endDate;
	private String[] score;

	protected Action action;

	public String alt(){
		if(isOnline()){
			addSessionToUser();
			if(user.getFlag()!=1){
				result.put("flag", "false");
				return SUCCESS;
			}
			action=(Action) hibernateSess.load(Action.class, getId()[0]);
			action.setCheckDate(getCheckDate()[0]);
			action.setDescription(getDescription()[0]);
			action.setEndDate(getEndDate()[0]);
			action.setTitle(getTitle()[0]);
			action.setScore(Integer.parseInt(getScore()[0]));
			action.setStartDate(getStartDate()[0]);
			action.setTid(getTid()[0]);
			hibernateSess.save(action);
			tx.commit();
			result.put("flag","true");
		}
		result.put("flag", "false");
		return SUCCESS;
	}
	public String add(){
		if(isOnline()
				&&getTitle()[0]!=null
				&&getStartDate()[0]!=null
				&&getCheckDate()[0]!=null
				&&getEndDate()[0]!=null)
		{
			addSessionToUser();
			if(user.getFlag()==1){
				action=new Action();
				action.setCheckDate(getCheckDate()[0]);
				action.setDescription(getDescription()[0]);
				action.setEndDate(getEndDate()[0]);
				action.setTitle(getTitle()[0]);
				action.setStartDate(getStartDate()[0]);
				action.setTid((String) request.getSession().getAttribute("userId"));
				action.setScore(Integer.parseInt(getScore()[0]));
				hibernateSess.save(action);
				tx.commit();
				result.put("actionId", action.getId());
				result.put("flag", "true");
				return SUCCESS;
			}
		}
		result.put("flag", "false");
		return SUCCESS;
	}
	//下面这个函数我看不出来有什么存在的必要啊……
	public Action add(String userId){
		if(isOnline()
				&&getTitle()[0]!=null
				&&getStartDate()[0]!=null
				&&getCheckDate()[0]!=null
				&&getEndDate()[0]!=null)
		{
			action=new Action();
			action.setDescription(userId);
			action.setTitle("0");
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			action.setStartDate(df.format(new Date()));
			
			Calendar cal = Calendar.getInstance();
			String[] endDate=df.format(new Date()).split("-");
			if(cal.MONTH+1<=6)
				endDate[1]="7".toString();
			else{
				endDate[0]=String.valueOf(cal.YEAR);
				endDate[1]="3".toString();
			}

			action.setCheckDate(df.format(new Date()));
			action.setEndDate(endDate[0]+"-"+endDate[1]+"-"+endDate[2]);
			action.setTid("999999999");
			hibernateSess.save(action);
			tx.commit();
			result.put("flag", "true");
			return action;
		}
		return null;
	}
	public String[] getDescription() {
		return description;
	}
	public void setDescription(String[] description) {
		this.description = description;
	}
	public String[] getStartDate() {
		return startDate;
	}
	public void setStartDate(String[] startDate) {
		this.startDate = startDate;
	}
	public String[] getCheckDate() {
		return checkDate;
	}
	public void setCheckDate(String[] checkDate) {
		this.checkDate = checkDate;
	}
	public String[] getEndDate() {
		return endDate;
	}
	public void setEndDate(String[] endDate) {
		this.endDate = endDate;
	}
	public String[] getId() {
		return id;
	}
	public void setId(String[] id) {
		this.id = id;
	}
	public String[] getTid() {
		return tid;
	}
	public void setTid(String[] tid) {
		this.tid = tid;
	}
	public String[] getScore() {
		return score;
	}
	public void setScore(String[] score) {
		this.score = score;
	}
	public String[] getTitle() {
		return title;
	}
	public void setTitle(String[] title) {
		this.title = title;
	}
	@Override
	@JSON(name = "result")
	public Map<String, String> getResult() {
		// TODO 自动生成的方法存根
		return result;
	}
}
