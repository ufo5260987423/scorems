package com.FileManager;

import java.io.File;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

public abstract class Basic extends com.logistic.Basic {
	private static final long serialVersionUID = 1L;
	/**
	 * @Title Basic.java
	 * @Package com.HistoryLocation.FileManager
	 * @author ufo
	 */
	protected File file;
	protected HttpServletRequest request;
	protected InputStream inputStream;
	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO 自动生成的方法存根
		this.request=request;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}
}
