package com.FileManager;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;

import org.apache.struts2.json.annotations.JSON;

public class Downloader extends Basic{
	private static final long serialVersionUID = 1L;
	/**
	 * @title Downloader.java
	 * @package com.HistoryLocation.FileManager
	 * @author ufo
	 */

	private String savePath;

	public String download(){
			try {
				if(isOnline()){
					String name=request.getParameterValues("name")[0];
					FileInputStream fis=new FileInputStream(getSavePath()+name);
					inputStream=fis;
					return SUCCESS;
				}
			} catch (Exception e) {
				// TODO 自动生成的 catch 块
				e.printStackTrace();
				return ERROR;
			}
			return ERROR;
	}
	public InputStream echo() {
		return inputStream;
	}
	public String getSavePath() {
		return savePath;
	}
	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}
	@Override
	@JSON(name = "result")
	public Map<String, String> getResult() {
		// TODO 自动生成的方法存根
		return result;
	}
}
