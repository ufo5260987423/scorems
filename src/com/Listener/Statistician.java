package com.Listener;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.json.annotations.JSON;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class Statistician extends ActionSupport implements Action{
	private static final long serialVersionUID = 1L;
	/**
	 * @title Statistician.java
	 * @package com.logistics.Listener
	 * @author ufo
	 */

	private Map<String,String> result=new HashMap<String,String>();
	protected HttpServletRequest request;
	public String onlineStastic(){
		ActionContext ctx=ActionContext.getContext();
		result.put("online", ctx.getApplication().get("online").toString());
		return SUCCESS;
	}
	@JSON(name="result")
	public Map<String,String> getResult() {
		return result;
	}
	public void setResult(Map<String,String> result) {
		this.result = result;
	}
}