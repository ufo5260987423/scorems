package com.Listener;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class OnlineListener implements HttpSessionListener{

	@Override
	public void sessionCreated(HttpSessionEvent sess) {
		// TODO 自动生成的方法存根
		HttpSession session=sess.getSession();
		ServletContext application=session.getServletContext();
		if(session.isNew()){
			application.setAttribute("online",
					1+Integer.parseInt
					(application.getAttribute("online").toString()));
		}
	}
	@Override
	public void sessionDestroyed(HttpSessionEvent sess) {
		// TODO 自动生成的方法存根
		HttpSession session=sess.getSession();
		ServletContext application=session.getServletContext();
		if(session.isNew())application.setAttribute("online",
				-1+Integer.parseInt
				((String)application.getAttribute("online")));
	}
}
