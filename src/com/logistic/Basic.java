package com.logistic;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.json.annotations.JSON;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.DAO.User;
import com.opensymphony.xwork2.ActionSupport;

public abstract class Basic extends ActionSupport implements ServletRequestAware{
	private static final long serialVersionUID = 1L;
	/**
	 * @Title Basic.java
	 * @Package com.logistic
	 * @author ufo
	 */

	protected Map<String,String> result=new HashMap<String,String>();
	protected Configuration conf=new Configuration().configure();
	protected SessionFactory sf=conf.buildSessionFactory();
	protected Session hibernateSess=sf.openSession();
	protected Transaction tx = hibernateSess.beginTransaction();
	protected HttpServletRequest request;
	protected User user;

	public boolean isOnline(){
		if(request.getSession().getAttribute("userId")!=null)
			return true;
		else 
			return false;
	}
	public void finalize() throws Throwable{
		hibernateSess.close();
		super.finalize();
	}
	@JSON(name="result")
	public abstract Map<String,String> getResult();
	public void setResult(Map<String,String> result){
		this.result=result;
	}
	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO 自动生成的方法存根
		this.request=request;
	}
	public void addSessionToUser(){
		user=(User) hibernateSess.load(User.class, (Serializable) request.getSession().getAttribute("userId"));
	}
}
